
import os
from ..src.mp3_file import MP3File
from mp3db.models import Hindi_Song

song_folder = "C:\Users\shalakak\Desktop\songs"

mp3_file_list = [os.path.join(os.path.abspath(song_folder), file_item) for file_item in os.listdir(os.path.abspath(song_folder)) if file_item.endswith(".mp3")]

for mp3_file in mp3_file_list:
    mp3_obj = MP3File(mp3_file)
    mp3_db_obj = Hindi_Song(title = mp3_obj.mp3_obj["TIT2"], \
                            artist = mp3_obj.mp3_obj["TPE1"], \
                            composer = mp3_obj.mp3_obj["TCOM"], \
                            album = mp3_obj.mp3_obj["TALB"], \
                            language = mp3_obj.mp3_obj["TLAN"], \
                            year = mp3_obj.mp3_obj["TDRC"])
    
    mp3_db_obj.save()