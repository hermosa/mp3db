
import os
from mutagen.mp3 import MP3

my_dir = "C:\songs"

def print_tags(file_name):
    
    s = MP3(file_name)
    for tag in s.tags:
        if tag != 'APIC:':
            print "\t", tag, s.tags[tag],
            print
            #print tag

def is_tag(file_name):

    s = MP3(file_name)
    if hasattr( s, 'tags' ):
       return True
    return False
    
def main():

    global my_dir
    
    for root, dir, file_list in os.walk(my_dir):
        for file_item in file_list:
            if file_item.endswith('.mp3'):
            # if not is_tag(os.path.join(root, file_item)):
                # print file_item
                print file_item
                print_tags( os.path.join(root, file_item) )
            
if __name__ == '__main__':
    main()