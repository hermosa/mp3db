from django.conf.urls import patterns, include, url

from django.contrib import admin
from mp3db import views
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', views.home),    
    url(r'addPage/$', views.add_page),
    url(r'addPrep/$', views.add_music_helper),
    url(r'add/$', views.add_music),
    url(r'display/$', views.display_db),
    url(r'search/$', views.search),
    url(r'syncDb/$', views.sync_db),
)
