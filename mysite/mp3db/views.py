
import os
import re
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.template import Template, Context
from django.shortcuts import render, render_to_response
from django.core.context_processors import csrf
from django.db import connection
from mysite.settings import DATABASES
from mp3db.models import *
from mp3db.forms import SongForm
from mp3_file import MP3File
from utils import *

class DummyObj():
    pass

def get_song_obj_list(song_list):
    
    song_obj_list = []
    for item in song_list:
        
        song = DummyObj()
        
        song.artists = []
        for art in item.artists.all():
            song.artists.append(art.name)
        if song.artists:
            song.artists = ';'.join(song.artists)
            
        song.composers = []
        for comp in item.composers.all():
            song.composers.append(comp.name)
        if song.composers:
            song.composers = ';'.join(song.composers)
            
        song.title = item.title
        song.album = item.album
        song.path = item.path
        song.year = item.year
            
        song_obj_list.append(song)
        
    return song_obj_list    



def home(request):
    return render_to_response('mp3db_home.html')
    
def add_music_helper(request):
   
    if not 'search_term' in request.GET or not request.GET['search_term']:
        return render(request, 'add_page.html', { 'error' : True, \
                                              'err_msg' : "Please submit a search term." })
    search_term = request.GET['search_term']
    
    if not is_valid_dir(search_term) and not is_mp3_file(search_term):
        return render_to_response('home.html', { 'error' : True, \
                                              'err_msg' : "Invalid name: %s. Please submit valid directory or mp3 file name." % search_term })
        
    songs_found_obj_list = []
    songs_not_found_obj_list = []
    song_names_not_found_list = []
        
    if is_mp3_file(search_term):
        
        song_file = search_term
        song_obj = MP3File(song_file)
        
        # Search for song in DB
        found1 = Song.objects.filter(title__iexact=song_obj.title)
        #found2 = Song.objects.filter(album__icontains=song_obj.album)
        #found3 = Song.objects.filter(artist__icontains=song_obj.artist)
        song_found = True if found1 else False 
        
        if song_found:
            songs_found_obj_list.append(song_obj)
        else:
            songs_not_found_obj_list.append(song_obj)
            song_names_not_found_list.append(search_term)
     
        
    elif is_valid_dir(search_term):
        
        try:
            for root, dirs, files in os.walk(search_term):
                
                for file_item in files:
                    
                    # Continue if not MP3 file
                    if not is_mp3_file(file_item):
                        continue
                    
                    # Instantiate MP3File class
                    song_path = os.path.join(root, file_item)
                    try:
                        song_obj = MP3File(song_path)
                        song_obj.path = song_path
                    except:
                        import pdb; pdb.set_trace()
                    
                    # Find in DB
                    found = Song.objects.filter(title__iexact=song_obj.title)
                    if 'comp_path' in request.GET:
                        found1 = Song.objects.filter(path__iexact=song_obj.mp3_obj.filename)
                        found = found & found1
                    #found3 = Song.objects.filter(artist__icontains=song_obj.artist)
                    #found = found1 & found2 & found3 
                    
                    if found:
                        found[0].path = song_path
                        songs_found_obj_list.append(song_obj)
                    else:
                        #=======================================================
                        # song_obj.artist = reduce(lambda temp, x: temp + " " + x, song_obj.artistList)
                        # song_obj.composer = reduce(lambda temp, x: temp + " " + x, song_obj.composerList)
                        #=======================================================
                        song_names_not_found_list.append(song_path)
                        songs_not_found_obj_list.append(song_obj)
            
            
    
        except IOError as e:
            return render_to_response('home.html', { 'error' : error, \
                                                  'err_msg' : "Exception occured." })
        
        if song_names_not_found_list:
            request.session['song_names_not_found_list'] = song_names_not_found_list        
        return render(request, 'display_db.html', { 'songs_found' : songs_found_obj_list,  \
                                                    'songs_not_found' : songs_not_found_obj_list, \
                                                    'song_dir' : search_term})
def add_music(request):
    
    if 'song_names_not_found_list' in request.session:
    
        song_names_not_found_list = request.session['song_names_not_found_list']
        #import pdb; pdb.set_trace()
        for song in song_names_not_found_list:
             
             mysong = Song()
             song_obj = MP3File(song)
             
             mysong.title = song_obj.title
             mysong.path = song_obj.mp3_obj.filename
             if song_obj.album:
                 mysong.album = song_obj.album
             if song_obj.year:
                 mysong.year = song_obj.year
                 
             # Add language if not in DB
             if song_obj.language:
                 obj, created = Language.objects.get_or_create(name=song_obj.language)
             else:
                 obj, created = Language.objects.get_or_create(name="")
             mysong.language = obj
             
             mysong.save()
             
             # Add artist if not in DB
             if song_obj.artists:
                 for artist_local in song_obj.artists.split(';'):
                     obj, created = Artist.objects.get_or_create(name=artist_local)
                     mysong.artists.add(obj)
                     
             # Add composer if not in DB
             if song_obj.composers:
                 for composer_local in song_obj.composers.split(';'):
                     obj, created = Composer.objects.get_or_create(name=composer_local)
                     mysong.composers.add(obj)
          
             mysong.save()
           
        return render_to_response('add_finish.html', {'song_num' : len(song_names_not_found_list)} ) 
          
    
    else:
        song_names_not_found_list = re.sub('__space__+', ' ', request.POST['song_names_not_found_list'])
        t = Template("<html><body>It is now {{ song_names_not_found_list }}.</body></html>")
        html = t.render(Context({'song_names_not_found_list':song_names_not_found_list}))
        return HttpResponse(html)
    
def search(request):
    
    if request.POST:
        
        
        found = []
        if 'title' in request.POST and request.POST['title']:
            found_temp = Song.objects.filter(title__icontains=request.POST['title'])
            found = found_temp
            
        if 'artist' in request.POST and request.POST['artist']:
            #import pdb; pdb.set_trace()
            found_temp = Song.objects.filter(artists__name__icontains=request.POST['artist'])
            if not found:
                found = found_temp
            else:
                found = found & found_temp
        if 'album' in request.POST and request.POST['album']:
            found_temp = Song.objects.filter(album__icontains=request.POST['album'])
            if not found:
                found = found_temp
            else:
                found = found & found_temp
                
        if 'composer' in request.POST and request.POST['composer']:
            found_temp = Song.objects.filter(composers__name__icontains=request.POST['composer'])
            if not found:
                found = found_temp
            else:
                found = found & found_temp
            
        if 'year' in request.POST and request.POST['year']:
            found_temp = Song.objects.filter(year__exact=request.POST['year'])
            if not found:
                found = found_temp
            else:
                found = found & found_temp
                
        if 'language' in request.POST and request.POST['language']:
            found_temp = Song.objects.filter(language__name__iexact=request.POST['language'])
            if not found:
                found = found_temp
            else:
                found = found & found_temp
        
        song_obj_list = get_song_obj_list(found)
                 
        return render_to_response('display_db.html', { 'songs_found' : song_obj_list })
    
    else:
        
        args = {}
        args.update(csrf(request))
        
        # Get language list
        cursor = connection.cursor()
        cursor.execute("SELECT DISTINCT name FROM mp3db_language")
        lang_list = cursor.fetchall()
        #import pdb; pdb.set_trace()
        args['lang_list'] = lang_list
        if lang_list:
            args['lang_list'] = [str(lang[0]).title() for lang in lang_list]
            args['lang_list'].insert(0, "")
        
        return render_to_response('search.html', args)
    
def display_db(request):
    
    song_obj_list = get_song_obj_list(Song.objects.all())
        
    return render_to_response('display_db.html', { 'songs_found' :  song_obj_list })
 
def add_page(request):
     return render_to_response('add_page.html')
 
def sync_db(request):
    
    for song in Song.objects.all():
        
        # Check if file exists
        try:
            with open(song.path) as f:
                pass
        except:
            continue
        
        song_obj = MP3File(song.path)
        
        
        
            
    