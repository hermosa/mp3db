
from django.forms import ModelForm, TextInput, ModelMultipleChoiceField, Field, ValidationError
from mp3db.models import *
        

class ModelSemicolonSeparatedMultipleChoiceField(Field):
    
    def to_python(self, value):
        
        try:
            # Return an empty list if no input was given.
            if not value:
                return []
            return [item.strip() for item in value.split(";")]
        except:
            raise ValidationError("Invalid value")
   
    
    def clean(self, value):
        if value is not None:
            value = [item.strip() for item in value.split(";")] # remove padding
        #return super(ModelSemicolonSeparatedMultipleChoiceField, self).clean(value)
        return value
    
         

class SongForm(ModelForm):
    
    artists = ModelSemicolonSeparatedMultipleChoiceField(
            required=False,
            #queryset=Artist.objects.filter(),
            #queryset=[],
            #to_field_name='name',
            widget = TextInput
    )
    
    composers = ModelSemicolonSeparatedMultipleChoiceField(
            required=False,
            #queryset=Composer.objects.filter(),
            #queryset=[], 
            #to_field_name='name',
            widget = TextInput
    )
    
    class Meta:
        model = Song
        
        
        #artists = forms.CharField(widget=forms.TextInput)
        #composers = forms.CharField(widget=forms.TextInput)
        exclude = ('path',)
        #fields = ('title', 'album', 'year', 'language', 'artists', 'composers')
        #=======================================================================
        # widgets = {
        #      'artists' : TextInput(),
        #      'composers' : TextInput(),     
        # }
        #=======================================================================
    