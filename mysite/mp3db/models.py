
import re
from django.core.exceptions import ValidationError
from django.db import models
from mp3_file import MP3File

class MyValidators(object):
    
    @classmethod
    def validate_year(cls, value):
        
        if not re.search('^\d\d\d\d$', value):
            raise ValidationError("Invalid year")
        

class Person(object):
    name = models.CharField(max_length=50, null=True, blank=True)
    country_of_origin = models.CharField(max_length=50, null=True, blank=True)
    
    def __unicode__(self):
        return self.name

class Language(models.Model):
    name = models.CharField(max_length=30, unique=True)
    
    def __unicode__(self):
        return self.name
    
class Composer(models.Model, Person):
    name = models.CharField(max_length=50, unique=True)
    country_of_origin = models.CharField(max_length=50, blank=True)
    
    def __unicode__(self):
        return self.name

    
class Artist(models.Model, Person):
    name = models.CharField(max_length=50, unique=True)
    country_of_origin = models.CharField(max_length=50, blank=True)
    
    def __unicode__(self):
        return self.name


class Song(models.Model):
    
    title = models.CharField(max_length=100, blank=True)
    path = models.CharField(max_length=300)
    album = models.CharField(max_length=100, blank=True)
    composers = models.ManyToManyField(Composer, null=True, blank=True)
    artists = models.ManyToManyField(Artist, null=True, blank=True)
    year = models.CharField(max_length=20, blank=True, validators=[MyValidators.validate_year])
    language = models.ForeignKey(Language, null=True, blank=True)
    
    
    def __unicode__(self):
        return self.title
    

    
