from django.conf.urls import patterns, include, url
from django.views.decorators.cache import cache_page

from django.contrib import admin
from id3tagger import views
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', views.home),
    url(r'^changeTag/$', views.change_tag_helper),
	url(r'^changeTag=(?P<search_term>.*)', views.change_tag_helper),
    url(r'^changeTagForm=(?P<song_path>.*)', views.change_tag),
    #url(r'^changedTag=(?P<song_path>.*)', views.change_tag_over),
    
)
#cache_page(60*15)