
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import Template, Context
from django.shortcuts import render, render_to_response
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from utils import *
from mp3_file import MP3File
from mp3db.forms import SongForm

def home(request):
    return render_to_response('id3tagger_home.html')

def change_tag_helper(request, search_term=""):
    
    # Render page for first time
    if not search_term:
        return render(request, 'change_tag.html')
    
    # User submitted empty search
    if search_term and not request.GET['search_term']:
        return render(request, 'change_tag.html', { 'error' : True, \
                                              'err_msg' : "Please submit a search term." })
        
    search_term = request.GET['search_term']
    
    # User submitted invalid search term
    if not is_valid_dir(search_term) and not is_mp3_file(search_term):
        return render_to_response('change_tag.html', { 'error' : True, \
                                              'err_msg' : "Invalid name: %s. Please submit a valid path or MP3 file name." % search_term })
        
    songs_obj_list = []
    
    if is_mp3_file(search_term):
        
        song_file = search_term
        song_obj = MP3File(song_file)
        songs_obj_list.append(song_obj)
        
    elif is_valid_dir(search_term):
        
        try:
            for root, dirs, files in os.walk(search_term):
                for file_item in files:
                    
                    # Continue if not MP3 file
                    if not is_mp3_file(file_item):
                        continue
                    
                    # Instantiate MP3File class
                    song_path = os.path.join(root, file_item)
                    try:
                        song_obj = MP3File(song_path)
                        song_obj.path = song_path
                        #import pdb; pdb.set_trace()
                    except:
                        import pdb; pdb.set_trace()
                   
                    songs_obj_list.append(song_obj)
                    
            request.session['song_dir'] = search_term
                  
    
        except IOError as e:
            return render_to_response('change_tag.html', { 'error' : True, \
                                                  'err_msg' : "Exception occured." })
        #request.session['songs_found'] = songs_obj_list
        
        #import pdb; pdb.set_trace()
        return render(request, 'display_id3_db.html', { 'songs_found' : songs_obj_list,  \
                                                    'song_dir' : search_term})

def change_tag(request, song_path):
    
    myargs = {}
    song_obj = MP3File(song_path)
    song_obj.path = song_path
    myargs['song_obj'] = song_obj
    myargs['error'] = False
    
    if request.method == 'POST':
        #import pdb; pdb.set_trace()
        form = SongForm(request.POST)
        myargs['form'] = form
        
        if form.is_valid():
            myargs['form'] = form
            empty_field_list = [field for field in form.cleaned_data if not form.cleaned_data[field]]
            if len(empty_field_list) == len(form.cleaned_data.keys()):
                myargs['error'] = True
                myargs['err_msg'] = "Please fill at least one field."
                return render(request, 'change_tag_form.html', myargs)
            
            tag_dict =  { field : value for (field, value) in form.cleaned_data.iteritems() if value and value != ['']}
              
            song_obj.change_tag(tag_dict)
            del myargs['form']
            myargs['song_obj'] = MP3File(song_path)
            song_obj.path = song_path
            return render(request, 'change_tag_form.html', myargs)
            #return HttpResponseRedirect(reverse('id3tagger.views.change_tag_over', args=(song_path,)))
            #return render(request, 'id3tagger_tag_changed.html')
            
        else:
            myargs['error'] = True
            myargs['err_msg'] = form.errors.values()
            return render(request, 'change_tag_form.html', myargs)
     
    else:
        form = SongForm()
        myargs.update(csrf(request))
        myargs['form'] = form
        
    return render(request, 'change_tag_form.html', myargs)
    
#===============================================================================
# def change_tag_over(request, song_path):
#     
#     myargs = {}
#     song_obj = MP3File(song_path)
#     song_obj.path = song_path
#     myargs['song_obj'] = song_obj
#     myargs['error'] = False
#     return render(request, 'change_tag_form.html', myargs)
#===============================================================================

