
import os
import re

def is_valid_dir(my_dir):
    return os.path.isdir(my_dir)

def is_mp3_file(file_name):
    return str(file_name).lower().endswith('.mp3')

def del_mp3_ext(file_name):
    return (re.sub('\.[0-9a-zA-Z]+', '', file_name))