
import re
import os
from mutagen.mp3 import MP3
from mutagen.id3 import ID3, TIT2, TALB, TPE1, TPE2, COMM, TCOM, TCON, TDRC, TLAN, error

class MP3File(object):
    
    def __init__(self, file_name):
        
        self.mp3_obj = MP3(os.path.normpath(str(file_name)))
        self.encoding = 0
        
        # Title
        self.title = unicode(re.sub('\.[0-9a-zA-Z]+', '', os.path.basename(file_name)))
        if 'TIT2' in self.mp3_obj:
            self.title = self.mp3_obj['TIT2'].text[0]
            self.encoding = self.mp3_obj['TIT2'].encoding
       
        # Artist List
        self.artists = u''
        if 'TPE1' in self.mp3_obj:
            self.artists = self.mp3_obj['TPE1'].text[0]
            self.encoding = self.mp3_obj['TPE1'].encoding
                  
        
        # Album
        self.album = u''
        if 'TALB' in self.mp3_obj:
            self.album = self.mp3_obj['TALB'].text[0]
            self.encoding = self.mp3_obj['TALB'].encoding
   
        # Composer list
        self.composers = u''
        if 'TCOM' in self.mp3_obj:
            self.composers = self.mp3_obj['TCOM'].text[0]
            self.encoding = self.mp3_obj['TCOM'].encoding
        
        # Year
        self.year = u''      
        if 'TDRC' in self.mp3_obj:    
            self.year = self.mp3_obj['TDRC'].text[0]
            self.encoding = self.mp3_obj['TDRC'].encoding
        
        # Language
        self.language = u''
        if 'TLAN' in self.mp3_obj:  
            self.language = self.mp3_obj['TLAN'].text[0]
            self.encoding = self.mp3_obj['TLAN'].encoding
               
        try:
            self.mp3_obj.add_tags()
        except error:
            pass
        
    def __unicode__(self):
        return self.mp3_obj.filename
    
    def change_tag(self, tag_dict):

        if 'title' in tag_dict:
            self.mp3_obj.tags.add( TIT2(encoding=self.encoding, text=tag_dict['title']) )
        if 'artists' in tag_dict:
            artists = ';'.join(tag_dict['artists'])
            self.mp3_obj.tags.add( TPE1(encoding=self.encoding, text=[artists]) )
        if 'album' in tag_dict:
            self.mp3_obj.tags.add( TALB(encoding=self.encoding, text=tag_dict['album']) )
        if 'composers' in tag_dict:
            composers = ';'.join(tag_dict['composers'])
            self.mp3_obj.tags.add( TCOM(encoding=self.encoding, text=[composers]) )
        if 'year' in tag_dict:
            self.mp3_obj.tags.add( TDRC(encoding=self.encoding, text=tag_dict['year']) )
        if 'language' in tag_dict:
            self.mp3_obj.tags.add( TLAN(encoding=self.encoding, text=tag_dict['language']) )
        self.mp3_obj.tags.save()
                    
    #===========================================================================
    # def save_tags(self, song_dict):
    #     
    #     self.mp3_obj.tags.add( TIT2(encoding=0, text=song_dict['title']) )
    #     self.mp3_obj.tags.add( TPE1(encoding=0, text=song_dict['artist']) )
    #     self.mp3_obj.tags.add( TDRC(encoding=0, text=song_dict['year']) )
    #     self.mp3_obj.tags.add( TALB(encoding=0, text=song_dict['album']) )
    #     self.mp3_obj.tags.add( TCOM(encoding=0, text=song_dict['composer']) )
    #     self.mp3_obj.tags.add( TLAN(encoding=0, text=song_dict['language']) )
    #     self.mp3_obj.tags.save(self.mp3_obj.filename)
    #===========================================================================
        #=======================================================================
        # tags["TPE1"] = TPE1(encoding=3, text=song_dict['artist'])
        # tags["TDRC"] = TDRC(encoding=3, text=song_dict['year'])
        # tags["TALB"] = TALB(encoding=3, text=song_dict['album'])
        # tags["TCOM"] = TCOM(encoding=3, text=song_dict['composer'])
        # tags["TLAN"] = TLAN(encoding=3, text=song_dict['language'])
        # tags.save(self.filename)
        #=======================================================================
        

            