from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from mysite import settings
from mysite import views
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', views.home),
    url(r'^home', views.home),    
    url(r'^mp3db/', include('mp3db.urls')),
    url(r'^id3tagger/', include('id3tagger.urls')),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += staticfiles_urlpatterns()
