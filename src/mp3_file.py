
import os
import re
from mutagen.mp3 import MP3
from mutagen.id3 import ID3, TIT2, TALB, TPE1, TPE2, COMM, TCOM, TCON, TDRC, TLAN, error

class MP3File(object):
    
    def __init__(self, file_name):
        
        self.mp3_obj = MP3(file_name)

        self.title = self.mp3_obj['TIT2'] if 'TIT2' in self.mp3_obj else unicode(re.sub('\.[0-9a-zA-Z]+', '', os.path.basename(file_name)))
        self.artist = self.mp3_obj['TPE1'] if 'TPE1' in self.mp3_obj else u""
        self.album = self.mp3_obj['TALB'] if 'TALB' in self.mp3_obj else u""
        self.composer = self.mp3_obj['TCOM'] if 'TCOM' in self.mp3_obj else u""
        self.year = self.mp3_obj['TDRC'] if 'TDRC' in self.mp3_obj else u""
        self.language = self.mp3_obj['TLAN'] if 'TLAN' in self.mp3_obj else u""
         
        try:
            self.mp3_obj.add_tags()
        except error:
            pass
                    
    def save_tags(self, song_dict):
        
        self.mp3_obj.tags.add( TIT2(encoding=0, text=song_dict['title']) )
        self.mp3_obj.tags.add( TPE1(encoding=0, text=song_dict['artist']) )
        self.mp3_obj.tags.add( TDRC(encoding=0, text=song_dict['year']) )
        self.mp3_obj.tags.add( TALB(encoding=0, text=song_dict['album']) )
        self.mp3_obj.tags.add( TCOM(encoding=0, text=song_dict['composer']) )
        self.mp3_obj.tags.add( TLAN(encoding=0, text=song_dict['language']) )
        self.mp3_obj.tags.save(self.mp3_obj.filename)
        #=======================================================================
        # tags["TPE1"] = TPE1(encoding=3, text=song_dict['artist'])
        # tags["TDRC"] = TDRC(encoding=3, text=song_dict['year'])
        # tags["TALB"] = TALB(encoding=3, text=song_dict['album'])
        # tags["TCOM"] = TCOM(encoding=3, text=song_dict['composer'])
        # tags["TLAN"] = TLAN(encoding=3, text=song_dict['language'])
        # tags.save(self.filename)
        #=======================================================================
        

            