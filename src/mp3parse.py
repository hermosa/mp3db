'''
Created on Feb 4, 2014

@author: shalakak
'''

import os
from mp3_file import MP3File
#from songs import song_dict

song_dir = "F:\\mytunes\\songs\\12 YUVRAJ"

def main(song_dir):
    
    song_dir = os.path.normpath(song_dir)
    print song_dir
    
    # Prepare list of .mp3 files in folder
    mp3_file_list = []
    for root, dirs, files in os.walk(song_dir):
        for file_item in files:
            if str(file_item).endswith('.mp3'):
                mp3_file_list.append(os.path.join(root, file_item))

    song_obj_list = []
    
    # Instantiate class for each file
    print mp3_file_list
    for file_item in mp3_file_list:
        
        # Test if ID3 module works for file
        # Instantiate class
        mp3_obj = MP3File(file_item)
        #mp3_obj.save_tags(song_dict[file_item])
        song_obj_list.append(mp3_obj)
        
        
        
    for song_obj in song_obj_list:
        print song_obj.title, song_obj.artist, song_obj.album
        #print song_obj.mp3_obj["TIT2"], song_obj.mp3_obj["TPE1"], song_obj.mp3_obj["TALB"]
    

main(song_dir)
    