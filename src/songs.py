song_dict = {

    'Chaudhvin Ka Chand Ho.mp3' :
        {
            'title'    : u'Chaudhvin Ka Chand Ho',
            'album'    : u'Chaudhvin Ka Chand',
            'composer' : u'Ravi',
            'artist'   : u'Mohammmed Rafi',
            'year'     : u'1961',
            'language' : u'hindi',
             
        },
             
     'Main Zindagi Ka Saath Nibhata Chala Gaya.mp3' :
        {
            'title'    : u'Main Zindagi Ka Saath Nibhata Chala Gaya',
            'album'    : u'Hum Dono',
            'composer' : u'Jai Dev',
            'artist'   : u'Mohammmed Rafi',
            'year'     : u'1961',
            'language' : u'hindi',
            
        },
             
     'Mere Mehboob Tujhe Meri Muhabbat.mp3' :
        {
            'title'    : u'Mere Mehboob Tujhe Meri Muhabbat',
            'album'    : u'Mere Mehboob',
            'composer' : u'Naushad',
            'artist'   : u'Mohammed Rafi',
            'year'     : u'1963',
            'language' : u'hindi',
            
        },
             
     'Bahut Shukriya Badi Meherbani.mp3' :
        {
            'title'    : u'Bahut Shukriya Badi Meherbani',
            'album'    : u'Ek Musafir Ek Hasina',
            'composer' : u'O.P Nayyar',
            'artist'   : u'Mohammmed Rafi, Asha Bhonsle',
            'year'     : u'1962',
            'language' : u'hindi',
            
        },
             
     'Lag Ja Gale Se Phir.mp3' :
        {
            'title'    : u'Lag Ja Gale Se Phir',
            'album'    : u'Woh Kaun Thi',
            'composer' : u'Madan Mohan',
            'artist'   : u'Lata Mangeshkar',
            'year'     : u'1964',
            'language' : u'hindi',
            
        },
             
     'Aa Chal Ke Tujhe.mp3' :
        {
            'title'    : u'Aa Chal Ke Tujhe',
            'album'    : u'Door Gagan Ki Chhaon Mein',
            'composer' : u'Kishore Kumar',
            'artist'   : u'Kishore Kumar',
            'year'     : u'1964',
            'language' : u'hindi',
            
        },
             
     'Mere Samnewali Khidki Mein.mp3' :
        {
            'title'    : u'Mere Samnewali Khidki Mein',
            'album'    : u'Padosan',
            'composer' : u'R.D Burman',
            'artist'   : u'Kishore Kumar',
            'year'     : u'1968',
            'language' : u'hindi',
            
        },
             
     'Bol Radha Bol.mp3' :
        {
            'title'    : u'Bol Radha Bol',
            'album'    : u'Sangam',
            'composer' : u'Shankar Jaikishan',
            'artist'   : u'Mukesh',
            'year'     : u'1964',
            'language' : u'hindi',
            
        },
             
     'Hansta Hua Noorani Chehra.mp3' :
        {
            'title'    : u'Hansta Hua Noorani Chehra',
            'album'    : u'Parasmani',
            'composer' : u'Laxmikant Pyarelal',
            'artist'   : u'Lata Mangeshkar, Kamal Barot',
            'year'     : u'1963',
            'language' : u'hindi',
            
        },
             
     'Aaj Phir Jeene Ki Tamanna Hai.mp3' :
        {
            'title'    : u'Aaj Phir Jeene Ki Tamanna Hai',
            'album'    : u'Guide',
            'composer' : u'S.D Burman',
            'artist'   : u'Lata Mangeshkar',
            'year'     : u'1965',
            'language' : u'hindi',
            
        },
             
     'Mera To Jo Bhi Kadam.mp3' :
        {
            'title'    : u'Mera To Jo Bhi Kadam',
            'album'    : u'Dosti',
            'composer' : u'Laxmikant Pyarelal',
            'artist'   : u'Mohammmed Rafi',
            'year'     : u'1964',
            'language' : u'hindi',
            
        },
             
     'Pukarta Chala Hoon Main.mp3' :
        {
            'title'    : u'Pukarta Chala Hoon Main',
            'album'    : u'Mere Sanam',
            'composer' : u'O.P Nayyar',
            'artist'   : u'Mohammmed Rafi',
            'year'     : u'1965',
            'language' : u'hindi',
            
        },
             
     'Mere Sapnon Ki Rani.mp3' :
        {
            'title'    : u'Mere Sapnon Ki Rani',
            'album'    : u'Aradhana',
            'composer' : u'S.D Burman',
            'artist'   : u'Kishore Kumar',
            'year'     : u'1969',
            'language' : u'hindi',
            
        },
             
     'Hothon Mein Aisi Baat.mp3' :
        {
            'title'    : u'Hothon Mein Aisi Baat',
            'album'    : u'Jewel Thief',
            'composer' : u'S.D Burman',
            'artist'   : u'Lata Mangeshkar',
            'year'     : u'1967',
            'language' : u'hindi',
            
        },
             
     'Bindiya Chamkegi.mp3' :
        {
            'title'    : u'Bindiya Chamkegi',
            'album'    : u'Do Raaste',
            'composer' : u'Laxmikant Pyarelal',
            'artist'   : u'Lata Mangeshkar',
            'year'     : u'1969',
            'language' : u'hindi',
            
        }
             
             

}