'''
Created on Feb 20, 2014

@author: shalakak
'''

import sqlite3 as lite
import sys
from songs import song_dict

song_keys = ['title', 'album', 'composer', 'artist', 'year', 'language']
DB_LOC = 'C:\\song_db\\songs.db'


def get_song_tuple(song_dict):
    
    song_list = []
    i = 1
    for song in song_dict:
        temp =  [i] + [song_dict[song][key] for key in song_keys]
        song_list.append(temp)
        i += 1
        
    return tuple(song_list)

con = lite.connect(DB_LOC)
 
with con:
    
    song_tuple = get_song_tuple(song_dict)
     
    cur = con.cursor()    
     
    #cur.execute("DROP TABLE IF EXISTS Hindi_Songs")
    #cur.execute("CREATE TABLE All_Songs(Id INT, Title TEXT, Album TEXT, Composer TEXT, Artist TEXT, Year TEXT, Language TEXT)")
    cur.execute("INSERT INTO mp3db_song VALUES(?, ?, ?, ?, ?, ?, ?)", song_tuple)
    

